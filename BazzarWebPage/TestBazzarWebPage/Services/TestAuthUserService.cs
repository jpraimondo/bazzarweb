﻿using BazzarWebPage.Application.Services;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using BazzarWebPage.Infraestructure.Repositories;
using Infraestructure.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBazzarWebPage.Helpers;
using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Core.Entities;
using Xunit;
using Xunit.Sdk;
using Microsoft.AspNetCore.Http;

namespace TestBazzarWebPage.Services
{
    public class TestAuthUserService : IDisposable
    {
        private static AppDbContext? _context;
        public static IUnitOfWork? _unitOfWork;
        public static AuthUsersService? _authUserServices;
  



        public TestAuthUserService()
        {

            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(databaseName: "MemoryDBTest").Options;
            _context = new AppDbContext(options);
            _unitOfWork = new UnitOfWork(_context);
            _authUserServices = new(_unitOfWork);

            if (!_context.Users.Any())
            {
                Users createUser = new Users()
                {
                    Name = "Juan",
                    Surname = "Perez",
                    Email = "jperez@correo.com",
                    HashPassword = BCrypt.Net.BCrypt.HashPassword("MiPassword"),
                    Birthday = DateTime.Now.AddYears(-32),
                    PhoneNumber = "24622451",
                    RolId = 1
                    
                };
                _unitOfWork.UsersRepository.Insert(createUser);
                _unitOfWork.Save();
            }
             


        }

        public void Dispose()
        {
            _context.Dispose();
        }


        [Fact]
        public async void ShouldInsertUsers()
        {
            //Arrange

            CreateUsersDto createUser = new CreateUsersDto()
            {
                Name = "Juan",
                Surname = "Perez",
                Email = "jperez@correo.com",
                Password = "MiPassword",
                Birthday = DateTime.Now.AddYears(-32),
                PhoneNumber = "24622451"
            };

            //Act

            var createdUser = await _authUserServices.Register(createUser);




            //Assert

            Assert.IsType<Users>(createdUser);

            
        }


        //[Theory]
        //[InlineData("otro@correo.com", "OtroPassword")]
        //[InlineData("jperez@correo.com", "MiPassword")]
        //public async void ShouldLoginUsers(string email, string password)
        //{
        //    //Arrange

        //    LoginDto login = new LoginDto()
        //    {
        //        Email = email,
        //        Password = password,
        //    };

        //    //Act

        //    var result = await _authUserServices.Logon(login);

        //    //Assert

        //    Assert.IsType<bool>(result);
            
        //    if(email.Equals("jperez@correo.com"))
        //    {
        //        Assert.True(result);
        //    }
        //    else
        //    {
        //        Assert.False(result);
        //    }
        //}


    }
}
