﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.Services;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using TestBazzarWebPage.Helpers;

namespace TestBazzarWebPage.Services
{
    public class TestSubCategoryService : IDisposable
    {
        private readonly AppDbContext? _context;
        public readonly IUnitOfWork? _unitOfWork;
        public readonly SubCategoriaService? _subCategoryServices;



        public TestSubCategoryService()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(databaseName: "MemoryDBTest").Options;
            _context = new AppDbContext(options);
            _unitOfWork = new UnitOfWork(_context);
            _subCategoryServices = new SubCategoriaService(_unitOfWork);

            if (!_context.SubCategorias.Any())
            {
                SeedDbContext.SeedDataCategory(_context);
                SeedDbContext.SeedDataSubCategory(_context);
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }


        [Fact]
        public async void ShouldInsertSubCategory()
        {
            //Arrange

            CreateSubCategoryDto category = new CreateSubCategoryDto()
            {
                Nombre = "Sub Categoria 9",
                CategoriaPadreId = 3
            };

            int idMax = _context.SubCategorias.Max(c => c.Id);

            //Act

            var createSubCategory = await _subCategoryServices.Insert(category);

            //Assert

            Assert.Equal(++idMax, createSubCategory.Id);

        }

        [Fact]
        public async void ShouldGetAllSubCategory()
        {

            //Arrange


            //Act

            var subCategories = await _subCategoryServices.GetAll();

            //Assert

            Assert.NotNull(subCategories);

            Assert.True(subCategories.Count() > 0);

        }

        [Theory]
        [InlineData(3)]
        [InlineData(99)]
        public async void ShouldGetByIdSubCategory(int id)
        {
            //Arrange


            //Act

            var subCategory = await _subCategoryServices.GetById(id);

            //Assert
            if (id == 3)
                Assert.NotNull(subCategory);
            else
                Assert.Null(subCategory);

        }

        [Theory]
        [InlineData(3, "SubCategoria 3 Editada")]
        public async void ShouldUpdateSubCategory(int id, string nombre)
        {

            //Arrange

            CreateSubCategoryDto category = new CreateSubCategoryDto()
            {
                Nombre = nombre,
                CategoriaPadreId = 2
            };

            //Act

            var createSubCategory = await _subCategoryServices.Update(category, id);

            //Assert

            Assert.Equal(3, createSubCategory.Id);

            Assert.True(createSubCategory.Nombre.Equals(nombre));

        }

        [Theory]
        [InlineData(3)]
        [InlineData(99)]
        public async void ShouldDeleteSubCategory(int id)
        {
            //Arrange


            //Act

            var result = await _subCategoryServices.Delete(id);

            SubCategory subCategory = await _subCategoryServices.GetById(id);



            //Assert
            if (id == 3)
            {
                Assert.True(result);
                Assert.True(subCategory.IsDelete);
            }
            else
            {
                Assert.False(result);
            }

        }


    }
}
