﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.Services;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using TestBazzarWebPage.Helpers;

namespace TestBazzarWebPage.Services
{
    public class TestDepartamentoService : IDisposable
    {

        private readonly AppDbContext? _context;
        public readonly IUnitOfWork? _unitOfWork;
        public readonly DepartamentoService? _departamentoServices;



        public TestDepartamentoService()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(databaseName: "MemoryDBTest").Options;
            _context = new AppDbContext(options);
            _unitOfWork = new UnitOfWork(_context);
            _departamentoServices = new DepartamentoService(_unitOfWork);

            if (!_context.Departamentos.Any())
                SeedDbContext.SeedDataDepartamento(_context);

        }

        public void Dispose()
        {
            _context.Dispose();
        }

        [Theory]
        [InlineData("Artigas")]
        [InlineData("Rivera")]
        public async void ShouldInsertDepartamento(string nombre)
        {
            //Arrange

            CreateDepartamentoDto departamento = new CreateDepartamentoDto()
            {
                Nombre = nombre,
            };

            int idMax = _context.Departamentos.Max(d => d.Id);

            //Act

            var createDepartamento = await _departamentoServices.Insert(departamento);





            //Assert

            Assert.Equal(nombre, createDepartamento.Nombre);

            Assert.Equal(++idMax, createDepartamento.Id);
        }



        [Fact]
        public async void ShouldGetAllDepartamentos()
        {

            //Arrange


            //Act

            var departamentos = await _departamentoServices.GetAll();

            //Assert

            Assert.NotNull(departamentos);

            Assert.True(departamentos.Count() > 0);

        }

        [Theory]
        [InlineData(5)]
        [InlineData(99)]
        public async void ShouldGetByIdDepartamento(int id)
        {
            //Arrange


            //Act

            var departamento = await _departamentoServices.GetById(id);

            //Assert
            if (id.Equals(5))
                Assert.NotNull(departamento);
            else
                Assert.Null(departamento);

        }

        [Theory]
        [InlineData(2, "Salto")]
        public async void ShouldUpdateDepartamento(int id, string nombre)
        {

            //Arrange

            CreateDepartamentoDto departamento = new CreateDepartamentoDto()
            {

                Nombre = nombre,

            };

            //Act

            var updateDepartamento = await _departamentoServices.Update(departamento, id);

            //Assert

            Assert.Equal(2, updateDepartamento.Id);

            Assert.True(updateDepartamento.Nombre.Equals("Salto"));

        }

        [Theory]
        [InlineData(3)]
        [InlineData(99)]
        public async void ShouldDeleteDepartamento(int id)
        {
            //Arrange


            //Act

            var result = await _departamentoServices.Delete(id);

            Departamento departamento = await _departamentoServices.GetById(id);



            //Assert
            if (id == 3)
            {
                Assert.True(result);
                Assert.True(departamento.IsDelete);
            }
            else
            {
                Assert.False(result);
            }

        }


    }
}
