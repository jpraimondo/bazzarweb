﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.Services;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using TestBazzarWebPage.Helpers;

namespace TestBazzarWebPage.Services
{
    public class TestRedesService : IDisposable
    {

        private readonly AppDbContext? _context;
        public readonly IUnitOfWork? _unitOfWork;
        public readonly RedesService? _redesServices;



        public TestRedesService()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(databaseName: "MemoryDBTest").Options;
            _context = new AppDbContext(options);
            _unitOfWork = new UnitOfWork(_context);
            _redesServices = new RedesService(_unitOfWork);

            if (!_context.Redes.Any())
            {
                SeedDbContext.SeedDataDepartamento(_context);
                SeedDbContext.SeedDataLocalidad(_context);
                SeedDbContext.SeedDataCategory(_context);
                SeedDbContext.SeedDataSubCategory(_context);
                SeedDbContext.SeedDataTienda(_context);
                SeedDbContext.SeedDataRedes(_context);
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }


        [Theory]
        [InlineData("Facebook", "https://www.facebook.com/nueva_empresa", 4)]
        [InlineData("Instagram", "https://www.instagram.com/nueva_empresa", 4)]
        public async void ShouldInsertRedes(string nombre, string link, int idTienda)
        {
            //Arrange

            CreateRedesDto red = new CreateRedesDto()
            {
                Nombre = nombre,
                Link = link,
                TiendaId = idTienda
            };

            //Act
            int idMax = _context.Redes.Max(r => r.Id);

            var createRedes = await _redesServices.Insert(red);

            //Assert

            Assert.Equal(link, createRedes.Link);

            Assert.Equal(++idMax, createRedes.Id);
        }



        [Fact]
        public async void ShouldGetAllRedes()
        {

            //Arrange


            //Act

            var redes = await _redesServices.GetAll();

            //Assert


            Assert.NotNull(redes);

            Assert.True(redes.Count() > 0);

        }

        [Theory]
        [InlineData(3)]
        [InlineData(99)]
        public async void ShouldGetByIdRed(int id)
        {
            //Arrange


            //Act

            var red = await _redesServices.GetById(id);

            //Assert
            if (id == 3)
                Assert.NotNull(red);
            else
                Assert.Null(red);

        }

        [Theory]
        [InlineData(6, "Facebook", "https://www.facebook.com/tienda_3_editada", 3)]
        [InlineData(5, "Instagram", "https://www.instagram.com/tienda_3_editada", 3)]
        public async void ShouldUpdateRedes(int id, string nombre, string link, int idTienda)
        {

            //Arrange

            CreateRedesDto redDto = new CreateRedesDto()
            {
                Nombre = nombre,
                Link = link,
                TiendaId = idTienda

            };

            //Act

            var updateRed = await _redesServices.Update(redDto, id);

            //Assert

            Assert.Equal(id, updateRed.Id);

            Assert.True(updateRed.Link.Equals(link));

        }

        [Theory]
        [InlineData(3)]
        [InlineData(99)]
        public async void ShouldDeleteRed(int id)
        {
            //Arrange


            //Act

            var result = await _redesServices.Delete(id);

            Redes red = await _redesServices.GetById(id);

            //Assert
            if (id == 3)
            {
                Assert.True(result);
                Assert.True(red.IsDelete);
            }
            else
            {
                Assert.False(result);
            }

        }



    }


}

