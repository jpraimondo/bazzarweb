﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.Services;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using TestBazzarWebPage.Helpers;

namespace TestBazzarWebPage.Services
{
    public class TestNewsletterService : IDisposable
    {
        private readonly AppDbContext? _context;
        public readonly IUnitOfWork? _unitOfWork;
        public readonly NewsletterService? _newsletterServices;



        public TestNewsletterService()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(databaseName: "MemoryDBTest").Options;
            _context = new AppDbContext(options);
            _unitOfWork = new UnitOfWork(_context);
            _newsletterServices = new NewsletterService(_unitOfWork);

            if (!_context.Newsletters.Any())
                SeedDbContext.SeedDataNewsletter(_context);

        }

        public void Dispose()
        {
            _context.Dispose();
        }


        [Theory]
        [InlineData("correo9@miempresa.com")]
        [InlineData("correo8@miempresa.com")]
        public async void ShouldInsertNewsletter(string correo)
        {
            //Arrange

            CreateNewsletterDto newsletter = new CreateNewsletterDto()
            {
                Correo = correo,
            };

            int idMax = _context.Newsletters.Max(n => n.Id);

            //Act

            var createNewsletter = await _newsletterServices.Insert(newsletter);

            //Assert

            Assert.Equal(correo, createNewsletter.Correo);

            Assert.Equal(++idMax, createNewsletter.Id);
        }



        [Fact]
        public async void ShouldGetAllNewsletters()
        {

            //Arrange


            //Act

            var newsletters = await _newsletterServices.GetAll();

            //Assert


            Assert.NotNull(newsletters);

            Assert.True(newsletters.Count() > 0);

        }

        [Theory]
        [InlineData(3)]
        [InlineData(99)]
        public async void ShouldGetByIdNewsletter(int id)
        {
            //Arrange


            //Act

            var newsletter = await _newsletterServices.GetById(id);

            //Assert
            if (id == 3)
                Assert.NotNull(newsletter);
            else
                Assert.Null(newsletter);

        }

        [Theory]
        [InlineData(2, "nuevo.correo2@mieempresa.com")]
        public async void ShouldUpdateDepartamento(int id, string correo)
        {

            //Arrange

            CreateNewsletterDto newsletter = new CreateNewsletterDto()
            {
                Correo = correo,

            };

            //Act

            var updateNewsletter = await _newsletterServices.Update(newsletter, id);

            //Assert

            Assert.Equal(2, updateNewsletter.Id);

            Assert.True(updateNewsletter.Correo.Equals(correo));

        }

        [Theory]
        [InlineData(3)]
        [InlineData(99)]
        public async void ShouldDeleteNewsletter(int id)
        {
            //Arrange


            //Act

            var result = await _newsletterServices.Delete(id);

            Newsletter newsletter = await _newsletterServices.GetById(id);

            //Assert
            if (id == 3)
            {
                Assert.True(result);
                Assert.True(newsletter.IsDelete);
            }
            else
            {
                Assert.False(result);
            }

        }



    }
}
