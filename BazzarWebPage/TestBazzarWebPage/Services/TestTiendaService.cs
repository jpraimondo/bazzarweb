﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.Services;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using TestBazzarWebPage.Helpers;

namespace TestBazzarWebPage.Services
{
    public class TestTiendaService : IDisposable
    {

        private readonly AppDbContext? _context;
        public readonly IUnitOfWork? _unitOfWork;
        public readonly TiendaService? _tiendaServices;


        public TestTiendaService()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(databaseName: "MemoryDBTest").Options;
            _context = new AppDbContext(options);
            _unitOfWork = new UnitOfWork(_context);
            _tiendaServices = new TiendaService(_unitOfWork);

            if (!_context.Tiendas.Any())
            {
                SeedDbContext.SeedDataDepartamento(_context);
                SeedDbContext.SeedDataLocalidad(_context);
                SeedDbContext.SeedDataCategory(_context);
                SeedDbContext.SeedDataSubCategory(_context);
                SeedDbContext.SeedDataTienda(_context);
            }

        }

        public void Dispose()
        {
            _context.Dispose();
        }


        [Theory]
        [InlineData("Nueva tienda 2", "Descripcion nueva tienda 2", 3, 2, "098943762")]
        [InlineData("Nueva tienda 2", "Descripcion otra nueva tienda 2", 4, 5, "098943662")]
        [InlineData("Nueva tienda 1", "Descripcion nueva tienda 1", 1, 2, "098456732")]
        public async void ShouldInsertTienda(string nombre, string descripcion, int localidadId, int subCategoryId, string telefono)
        {
            //Arrange

            CreateTiendaDto tienda = new CreateTiendaDto()
            {
                Nombre = nombre,
                Descripcion = descripcion,
                LocalidadId = localidadId,
                SubCategoriaId = subCategoryId,
                Telefono = telefono
            };

            int idMax = _context.Tiendas.Max(t => t.Id);

            //Act

            var createTienda = await _tiendaServices.Insert(tienda);


            //Assert

            Assert.Equal(nombre, createTienda.Nombre);

            Assert.Equal(++idMax, createTienda.Id);
        }



        [Fact]
        public async void ShouldGetAllTienda()
        {

            //Arrange


            //Act

            var tiendas = await _tiendaServices.GetAll();

            //Assert

            Assert.NotNull(tiendas);

            Assert.True(tiendas.Count() > 0);

        }

        [Theory]
        [InlineData(5)]
        [InlineData(99)]
        public async void ShouldGetByIdTienda(int id)
        {
            //Arrange


            //Act

            var tienda = await _tiendaServices.GetById(id);

            //Assert
            if (id.Equals(5))
                Assert.NotNull(tienda);
            else
                Assert.Null(tienda);

        }

        [Theory]
        [InlineData(4, "Editar tienda 4", "Descripcion editada tienda 4", 1, 2, "098456732")]
        public async void ShouldUpdateTienda(int idTienda, string nombre, string descripcion, int localidadId, int subCategoryId, string telefono)
        {

            //Arrange

            CreateTiendaDto tienda = new CreateTiendaDto()
            {
                Nombre = nombre,
                Descripcion = descripcion,
                LocalidadId = localidadId,
                SubCategoriaId = subCategoryId,
                Telefono = telefono
            };

            //Act

            var updateTienda = await _tiendaServices.Update(tienda, idTienda);

            //Assert

            Assert.Equal(4, updateTienda.Id);

            Assert.True(updateTienda.Nombre.Equals("Editar tienda 4"));

        }

        [Theory]
        [InlineData(3)]
        [InlineData(99)]
        public async void ShouldDeleteTienda(int id)
        {
            //Arrange


            //Act

            var result = await _tiendaServices.Delete(id);

            Tienda tienda = await _tiendaServices.GetById(id);

            //Assert
            if (id == 3)
            {
                Assert.True(result);
                Assert.True(tienda.IsDelete);
            }
            else
            {
                Assert.False(result);
            }

        }

    }
}
