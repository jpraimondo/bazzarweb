﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.Services;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using TestBazzarWebPage.Helpers;

namespace TestBazzarWebPage.Services
{
    public class TestLocalidadService : IDisposable
    {

        private readonly AppDbContext? _context;
        public readonly IUnitOfWork? _unitOfWork;
        public readonly LocalidadService? _localidadServices;



        public TestLocalidadService()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(databaseName: "MemoryDBTest").Options;
            _context = new AppDbContext(options);
            _unitOfWork = new UnitOfWork(_context);
            _localidadServices = new LocalidadService(_unitOfWork);

            if (!_context.Localidades.Any())
            {
                if (!_context.Departamentos.Any())
                    SeedDbContext.SeedDataDepartamento(_context);


                SeedDbContext.SeedDataLocalidad(_context);
            }

        }

        public void Dispose()
        {
            _context.Dispose();
        }

        [Theory]
        [InlineData("Chuy", 4)]
        [InlineData("Rocha", 4)]
        public async void ShouldInsertLocalidad(string nombreLocalidad, int idDepartamento)
        {
            //Arrange

            CreateLocalidadDto localidadDto = new CreateLocalidadDto()
            {
                Nombre = nombreLocalidad,
                DepartamentoId = idDepartamento
            };
            int idMax = _context.Localidades.Max(l => l.Id);

            //Act

            var createLocalidad = await _localidadServices.Insert(localidadDto);



            //Assert

            Assert.Equal(nombreLocalidad, createLocalidad.Nombre);

            Assert.Equal(++idMax, createLocalidad.Id);
        }



        [Fact]
        public async void ShouldGetAllLocalidad()
        {

            //Arrange


            //Act

            var localidades = await _localidadServices.GetAll();

            //Assert

            Assert.NotNull(localidades);

            Assert.True(localidades.Count() > 0);

        }

        [Theory]
        [InlineData(3)]
        [InlineData(99)]
        public async void ShouldGetByIdLocalidad(int id)
        {
            //Arrange


            //Act

            var localidad = await _localidadServices.GetById(id);

            //Assert
            if (id == 3)
                Assert.NotNull(localidad);
            else
                Assert.Null(localidad);

        }

        [Theory]
        [InlineData(3, "C. de la Costa", 2)]
        public async void ShouldUpdateLocalidad(int id, string nombre, int idDepartamento)
        {

            //Arrange

            CreateLocalidadDto localidad = new CreateLocalidadDto()
            {
                Nombre = nombre,
                DepartamentoId = idDepartamento

            };

            //Act

            var createLocalidad = await _localidadServices.Update(localidad, id);

            //Assert

            Assert.Equal(3, createLocalidad.Id);

            Assert.True(createLocalidad.Nombre.Equals("C. de la Costa"));

        }

        [Theory]
        [InlineData(4)]
        [InlineData(99)]
        public async void ShouldDeleteLocalidad(int id)
        {
            //Arrange


            //Act

            var result = await _localidadServices.Delete(id);

            Localidad localidad = await _localidadServices.GetById(id);



            //Assert
            if (id == 4)
            {
                Assert.True(result);
                Assert.True(localidad.IsDelete);
            }
            else
            {
                Assert.False(result);
            }

        }

    }
}
