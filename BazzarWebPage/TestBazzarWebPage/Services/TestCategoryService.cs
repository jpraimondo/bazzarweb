﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.Helpers;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Application.Services;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Infraestructure.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Moq;
using TestBazzarWebPage.Helpers;

namespace TestBazzarWebPage.Services
{

    public class TestCategoryService : IDisposable
    {
        private static AppDbContext? _context;
        public static IUnitOfWork? _unitOfWork;
        public static CategoryService? _categoryServices;
        public static IFileHelper? _fileHelper;
        public static Mock<IAlmacenadorArchivo>? mockAlmacenadorArchivo;



        public TestCategoryService()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(databaseName: "MemoryDBTest").Options;
            _context = new AppDbContext(options);
            _unitOfWork = new UnitOfWork(_context);
            mockAlmacenadorArchivo = new Mock<IAlmacenadorArchivo>();
            _fileHelper = new FileHelper(mockAlmacenadorArchivo.Object);
            _categoryServices = new(_unitOfWork, _fileHelper);

            if (!_context.Categorias.Any())
                SeedDbContext.SeedDataCategory(_context);


        }

        public void Dispose()
        {
            _context.Dispose();
        }


        [Fact]
        public async void ShouldInsertCategory()
        {
            //Arrange

            CreateCategoryDto category = new CreateCategoryDto()
            {
                Nombre = "Categoria 4",
            };

            int idMax = _context.Categorias.Max(c => c.Id);

            //Act

            var createCategory = await _categoryServices.Insert(category);

            //Assert

            Assert.Equal(idMax + 1, createCategory.Id);

        }

        [Fact]
        public async void ShouldGetAllCategory()
        {

            //Arrange


            //Act

            var categories = await _categoryServices.GetAll();

            //Assert

            Assert.NotNull(categories);

            Assert.True(categories.Count() > 0);

        }

        [Theory]
        [InlineData(3)]
        [InlineData(8)]
        public async void ShouldGetByIdCategory(int id)
        {
            //Arrange


            //Act

            var category = await _categoryServices.GetById(id);

            //Assert
            if (id == 3)
                Assert.NotNull(category);
            else
                Assert.Null(category);

        }

        [Theory]
        [InlineData(3, "Categoria Editada")]
        public async void ShouldUpdateCategory(int id, string nombre)
        {

            //Arrange

            CreateCategoryDto category = new CreateCategoryDto()
            {
                Nombre = nombre,

            };

            //Act

            var createCategory = await _categoryServices.Update(category, id);

            //Assert

            Assert.Equal(3, createCategory.Id);

            Assert.True(createCategory.Nombre.Equals(nombre));

        }

        [Theory]
        [InlineData(3)]
        [InlineData(8)]
        public async void ShouldDeleteCategory(int id)
        {
            //Arrange


            //Act

            var result = await _categoryServices.Delete(id);

            Category category = await _categoryServices.GetById(id);



            //Assert
            if (id == 3)
            {
                Assert.True(result);
                Assert.True(category.IsDelete);
            }
            else
            {
                Assert.False(result);
            }

        }



    }
}
