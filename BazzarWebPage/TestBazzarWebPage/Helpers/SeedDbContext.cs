﻿

using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;

namespace TestBazzarWebPage.Helpers
{
    public static class SeedDbContext
    {

        public static void SeedData(AppDbContext context)
        {

        }

        public static void SeedDataCategory(AppDbContext context)
        {
            var categories = new List<Category>(){
                new Category()
                {
                    Id=1,
                    IsActve=true,
                    IsDelete=false,
                    Nombre = "categoria 1",
                    UrlImagen= "Url de la categoria",

                },
                new Category()
                {
                    Id=2,
                    IsActve=true,
                    IsDelete=false,
                    Nombre = "categoria 2",
                    UrlImagen= "Url de la categoria",

                },new Category()
                {
                    Id=3,
                    IsActve=true,
                    IsDelete=false,
                    Nombre = "categoria 3",
                    UrlImagen= "Url de la categoria",

                },

                };

            context.AddRange(categories);
            context.SaveChanges();

        }


        public static void SeedDataDepartamento(AppDbContext context)
        {
            var departamentos = new List<Departamento>()
            {
                new Departamento() { Id=1, IsActve=true,Nombre="Montevideo",IsDelete=false},
                new Departamento() { Id=2, IsActve=true,Nombre="Canelones",IsDelete=false},
                new Departamento() { Id=3, IsActve=true,Nombre="Maldonado",IsDelete=false},
                new Departamento() { Id=4, IsActve=true,Nombre="Rocha",IsDelete=false},
                new Departamento() { Id=5, IsActve=true,Nombre="Colonia",IsDelete=false},
                new Departamento() { Id=6, IsActve=true,Nombre="San Jose",IsDelete=false},
                new Departamento() { Id=7, IsActve=true,Nombre="Flores",IsDelete=false},
            };

            context.AddRange(departamentos);
            context.SaveChanges();

        }

        public static void SeedDataLocalidad(AppDbContext context)
        {
            var localidades = new List<Localidad>()
            {
                new Localidad() { Id=1, IsActve=true,Nombre="Montevideo",IsDelete=false,DepartamentoId=1},
                new Localidad() { Id=2, IsActve=true,Nombre="Canelones",IsDelete=false,DepartamentoId=2},
                new Localidad() { Id=3, IsActve=true,Nombre="Ciudad de la Costa",IsDelete=false,DepartamentoId=2},
                new Localidad() { Id=4, IsActve=true,Nombre="Atlantida",IsDelete=false, DepartamentoId = 2},
                new Localidad() { Id=5, IsActve=true,Nombre="Maldonado",IsDelete=false, DepartamentoId = 3},
                new Localidad() { Id=6, IsActve=true,Nombre="Punta del Este",IsDelete=false, DepartamentoId = 3},
                new Localidad() { Id=7, IsActve=true,Nombre="Piriapolis",IsDelete=false, DepartamentoId = 3},
            };

            context.AddRange(localidades);
            context.SaveChanges();

        }

        public static void SeedDataNewsletter(AppDbContext context)
        {
            var newsletters = new List<Newsletter>()
            {
                new Newsletter() { Id=1, IsActve=true,Correo="correo1@empresa.com",IsDelete=false},
                new Newsletter() { Id=2, IsActve=true,Correo="correo2@empresa.com",IsDelete=false},
                new Newsletter() { Id=3, IsActve=true,Correo="correo3@empresa.com",IsDelete=false},
                new Newsletter() { Id=4, IsActve=true,Correo="correo4@empresa.com",IsDelete=false},
                new Newsletter() { Id=5, IsActve=true,Correo="correo5@empresa.com",IsDelete=false},
                new Newsletter() { Id=6, IsActve=true,Correo="correo6@empresa.com",IsDelete=false},
                new Newsletter() { Id=7, IsActve=true,Correo="correo7@empresa.com",IsDelete=false},
            };

            context.AddRange(newsletters);
            context.SaveChanges();

        }


        public static void SeedDataRedes(AppDbContext context)
        {
            var redes = new List<Redes>()
            {
                new Redes() { Id=1, IsActve=true,Nombre="Instagram", Link="http://instagram.com/tienda1",TiendaId=1, IsDelete=false},
                new Redes() { Id=2, IsActve=true,Nombre="Facebook", Link="http://facebook.com/tienda1",TiendaId=1, IsDelete=false},
                new Redes() { Id=3, IsActve=true,Nombre="Instagram", Link="http://instagram.com/tienda2",TiendaId=2, IsDelete=false},
                new Redes() { Id=4, IsActve=true,Nombre="Facebook", Link="http://facebook.com/tienda2",TiendaId=2, IsDelete=false},
                new Redes() { Id=5, IsActve=true,Nombre="Instagram", Link="http://instagram.com/tienda3",TiendaId=3, IsDelete=false},
                new Redes() { Id=6, IsActve=true,Nombre="Facebook", Link="http://facebook.com/tienda3",TiendaId=3, IsDelete=false},
                new Redes() { Id=7, IsActve=true,Nombre="Facebook", Link="http://facebook.com/tienda4",TiendaId=4, IsDelete=false},
            };

            context.AddRange(redes);
            context.SaveChanges();

        }

        public static void SeedDataSubCategory(AppDbContext context)
        {
            var subCategories = new List<SubCategory>(){
                new SubCategory(){Id=1,IsActve=true,IsDelete=false,Nombre = "SubCategoria 1",UrlImagen= "Url de la SubCategoria", CategoriaPadreId = 1},
                new SubCategory(){Id=2,IsActve=true,IsDelete=false,Nombre = "SubCategoria 2",UrlImagen= "Url de la SubCategoria", CategoriaPadreId = 1},
                new SubCategory(){Id=3,IsActve=true,IsDelete=false,Nombre = "SubCategoria 3",UrlImagen= "Url de la SubCategoria", CategoriaPadreId = 2},
                new SubCategory(){Id=4,IsActve=true,IsDelete=false,Nombre = "SubCategoria 4",UrlImagen= "Url de la SubCategoria", CategoriaPadreId = 2},
                new SubCategory(){Id=5,IsActve=true,IsDelete=false,Nombre = "SubCategoria 5",UrlImagen= "Url de la SubCategoria", CategoriaPadreId = 2},
                new SubCategory(){Id=6,IsActve=true,IsDelete=false,Nombre = "SubCategoria 6",UrlImagen= "Url de la SubCategoria", CategoriaPadreId = 2},
                new SubCategory(){Id=7,IsActve=true,IsDelete=false,Nombre = "SubCategoria 7",UrlImagen= "Url de la SubCategoria", CategoriaPadreId = 3},
                new SubCategory(){Id=8,IsActve=true,IsDelete=false,Nombre = "SubCategoria 8",UrlImagen= "Url de la SubCategoria", CategoriaPadreId = 3},
                };

            context.AddRange(subCategories);
            context.SaveChanges();

        }

        public static void SeedDataTienda(AppDbContext context)
        {
            var tiendas = new List<Tienda>(){
                new Tienda(){Id=1,IsActve=true,IsDelete=false,Nombre = "Tienda 1",Descripcion= "Descripcion Tienda 1", SubCategoriaId = 1,LocalidadId = 1,Slug="http://empresa.com/tienda-1"},
                new Tienda(){Id=2,IsActve=true,IsDelete=false,Nombre = "Tienda 2",Descripcion= "Descripcion Tienda 2", SubCategoriaId = 1,LocalidadId = 1,Slug="http://empresa.com/tienda-2"},
                new Tienda(){Id=3,IsActve=true,IsDelete=false,Nombre = "Tienda 3",Descripcion= "Descripcion Tienda 3", SubCategoriaId = 2,LocalidadId = 5,Slug="http://empresa.com/tienda-3"},
                new Tienda(){Id=4,IsActve=true,IsDelete=false,Nombre = "Tienda 4",Descripcion= "Descripcion Tienda 4", SubCategoriaId = 2,LocalidadId = 5,Slug="http://empresa.com/tienda-4"},
                new Tienda(){Id=5,IsActve=true,IsDelete=false,Nombre = "Tienda 5",Descripcion= "Descripcion Tienda 5", SubCategoriaId = 2,LocalidadId = 4, Slug = "http://empresa.com/tienda-5"},
                new Tienda(){Id=6,IsActve=true,IsDelete=false,Nombre = "Tienda 6",Descripcion= "Descripcion Tienda 6", SubCategoriaId = 2,LocalidadId = 4, Slug = "http://empresa.com/tienda-6"},
                new Tienda(){Id=7,IsActve=true,IsDelete=false,Nombre = "Tienda 7",Descripcion= "Descripcion Tienda 7", SubCategoriaId = 3,LocalidadId = 5, Slug = "http://empresa.com/tienda-7"},
                new Tienda(){Id=8,IsActve=true,IsDelete=false,Nombre = "Tienda 8",Descripcion= "Descripcion Tienda 8", SubCategoriaId = 3,LocalidadId = 4, Slug = "http://empresa.com/tienda-8"},
                };

            context.AddRange(tiendas);
            context.SaveChanges();

        }


    }
}
