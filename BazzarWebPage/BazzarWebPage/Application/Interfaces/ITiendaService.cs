﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Interfaces
{
    public interface ITiendaService
    {

        Task<IEnumerable<ViewTiendaDto>> GetAll();
        Task<IEnumerable<ViewTiendaDto>> GetAllAndDelete();
        Task<Tienda> GetById(int id);
        Task<IEnumerable<ViewTiendaDto>> Find(Expression<Func<Tienda, bool>> predicate);
        public Task<Tienda> Insert(CreateTiendaDto tiendaDto);
        public Task<Tienda> Update(CreateTiendaDto tiendaDto, int id);
        Task<bool> Delete(int id);
    }
}
