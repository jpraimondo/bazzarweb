﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;


namespace BazzarWebPage.Application.Interfaces
{
    public interface INewsletterService
    {

        Task<IEnumerable<ViewNewsletterDto>> GetAll();
        Task<IEnumerable<ViewNewsletterDto>> GetAllAndDelete();
        Task<Newsletter> GetById(int id);
        IEnumerable<ViewNewsletterDto> Find(Expression<Func<Newsletter, bool>> predicate);
        public Task<Newsletter> Insert(CreateNewsletterDto newsletterDto);
        public Task<Newsletter> Update(CreateNewsletterDto newsletterDto, int id);
        Task<bool> Delete(int id);
    }
}
