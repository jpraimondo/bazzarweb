﻿using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazzarWebPage.Application.Interfaces
{
    public interface IFileHelper : IDisposable
    {

        Task<string> SaveImage(IBrowserFile image, string? contenedor);
        Task<string> AdjustSaveImage(IBrowserFile image, int maxWidth, int maxHeight, string? contenedor);

    }
}
