﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Interfaces
{
    public interface ILocalidadService
    {
        Task<IEnumerable<ViewLocalidadDto>> GetAll();
        Task<IEnumerable<ViewLocalidadDto>> GetAllAndDelete();
        Task<Localidad> GetById(int id);
        IEnumerable<Localidad> Find(Expression<Func<Localidad, bool>> predicate);
        public Task<Localidad> Insert(CreateLocalidadDto localidadDto);
        public Task<Localidad> Update(CreateLocalidadDto localidadDto, int id);
        Task<bool> Delete(int id);

    }
}
