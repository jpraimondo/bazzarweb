﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Interfaces
{
    public interface ISubCategoriaService
    {

        Task<IEnumerable<ViewSubCategoryDto>> GetAll();
        Task<IEnumerable<ViewSubCategoryDto>> GetAllAndDelete();
        Task<SubCategory> GetById(int id);
        IEnumerable<SubCategory> Find(Expression<Func<SubCategory, bool>> predicate);
        public Task<SubCategory> Insert(CreateSubCategoryDto subcategoryDto);
        public Task<SubCategory> Update(CreateSubCategoryDto subcategoryDto, int id);
        Task<bool> Delete(int id);
    }
}
