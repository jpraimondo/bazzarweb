﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Interfaces
{
    public interface IDepartamentoService
    {

        Task<IEnumerable<ViewDepartamentoDto>> GetAll();
        Task<IEnumerable<ViewDepartamentoDto>> GetAllAndDelete();
        Task<Departamento> GetById(int id);
        IEnumerable<Departamento> Find(Expression<Func<Departamento, bool>> predicate);
        public Task<Departamento> Insert(CreateDepartamentoDto departamentoDto);
        public Task<Departamento> Update(CreateDepartamentoDto departamentoDto, int id);
        Task<bool> Delete(int id);
    }
}
