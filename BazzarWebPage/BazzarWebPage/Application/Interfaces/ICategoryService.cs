﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Interfaces
{
    public interface ICategoryService
    {

        Task<IEnumerable<ViewCategoryDto>> GetAll();
        Task<IEnumerable<ViewCategoryDto>> GetAllAndDelete();
        Task<Category> GetById(int id);
        IEnumerable<Category> Find(Expression<Func<Category, bool>> predicate);
        public Task<Category> Insert(CreateCategoryDto categoryDto);
        public Task<Category> Update(CreateCategoryDto categoryDto, int id);
        Task<bool> Delete(int id);
    }
}
