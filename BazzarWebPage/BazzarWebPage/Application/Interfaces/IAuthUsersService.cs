﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Core.Entities;
using System.Security.Claims;

namespace BazzarWebPage.Application.Interfaces
{
	public interface IAuthUsersService
	{
        Task<Users> Register(CreateUsersDto createUsers);
        Task<Users> Login(LoginDto userLogin);
    }
}
