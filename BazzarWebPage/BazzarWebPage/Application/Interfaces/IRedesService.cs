﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.DTOs.ViewDetail;
using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Interfaces
{
    public interface IRedesService
    {
        Task<IEnumerable<ViewRedesDto>> GetAll();
        Task<IEnumerable<ViewRedesDto>> GetAllAndDelete();
        Task<Redes> GetById(int id);
        IEnumerable<Redes> Find(Expression<Func<Redes, bool>> predicate);
        public Task<ViewDetailRedesDto> Insert(CreateRedesDto redDto);
        public Task<ViewDetailRedesDto> Update(CreateRedesDto redDto, int id);
        Task<bool> Delete(int id);
    }
}
