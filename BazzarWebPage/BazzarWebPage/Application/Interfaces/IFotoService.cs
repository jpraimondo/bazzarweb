﻿using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Interfaces
{
    public interface IFotoService
    {

        Task<IEnumerable<Foto>> GetAll();
        Task<IEnumerable<Foto>> GetAllAndDelete();
        Task<Foto> GetById(int? id);
        IEnumerable<Foto> Find(Expression<Func<Foto, bool>> predicate);
        public Task<Foto> Insert(Foto contactDto);
        public Task<Foto> Update(Foto contactDto, int id);
        Task<bool> Delete(int id);
    }
}
