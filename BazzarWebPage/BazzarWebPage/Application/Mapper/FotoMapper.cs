﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;

namespace BazzarWebPage.Application.Mapper
{
    public class FotoMapper
    {

        public static Foto ToFoto(CreateFotoDto createFoto)
        {
            return new Foto()
            {
                Orden = createFoto.Orden,
            };
        }

        public static ViewFotoDto ToViewFotoDto(Foto foto)
        {
            return new ViewFotoDto()
            {
                UrlFoto = foto.UrlFoto,
                Orden = foto.Orden,
            };
        }
    }
}
