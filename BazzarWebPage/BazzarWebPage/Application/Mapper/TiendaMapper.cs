﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;

namespace BazzarWebPage.Application.Mapper
{
    public class TiendaMapper
    {
        public static Tienda ToTienda(CreateTiendaDto createTienda)
        {
            return new Tienda()
            {
                Nombre = createTienda.Nombre,
                Descripcion = createTienda.Descripcion,
                LocalidadId = createTienda.LocalidadId,
                SubCategoriaId = createTienda.SubCategoriaId,
                Telefono = createTienda.Telefono,
            };
        }


        public static ViewTiendaDto ToViewTiendaDto(Tienda tienda)
        {
            return new ViewTiendaDto()
            {
                Id = tienda.Id,
                Nombre = tienda.Nombre,
                Descripcion = tienda.Descripcion,
                Slug = tienda.Slug,
                Telefono = tienda.Telefono,
            };
        }

    }
}
