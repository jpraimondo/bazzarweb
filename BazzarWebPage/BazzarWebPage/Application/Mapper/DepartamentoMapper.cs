﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;

namespace BazzarWebPage.Application.Mapper
{
    public class DepartamentoMapper
    {


        public static Departamento ToDepartamento(CreateDepartamentoDto createDepartamento)
        {
            return new Departamento()
            {
                Nombre = createDepartamento.Nombre,

            };
        }


        public static ViewDepartamentoDto ToViewDepartamentoDto(Departamento departamento)
        {
            return new ViewDepartamentoDto()
            {
                Id = departamento.Id,
                Nombre = departamento.Nombre,
            };
        }
    }
}
