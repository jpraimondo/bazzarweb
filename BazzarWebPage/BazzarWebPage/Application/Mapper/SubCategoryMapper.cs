﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.DTOs.ViewDetail;
using BazzarWebPage.Core.Entities;

namespace BazzarWebPage.Application.Mapper
{
    public class SubCategoryMapper
    {
        public static SubCategory ToSubCategory(CreateSubCategoryDto createSubCategory)
        {
            return new SubCategory()
            {
                Nombre = createSubCategory.Nombre,
                CategoriaPadreId = createSubCategory.CategoriaPadreId,
            };
        }

        public static ViewSubCategoryDto ToViewSubCategoryDto(SubCategory subCategory)
        {
            return new ViewSubCategoryDto()
            {
                Id = subCategory.Id,
                Nombre = subCategory.Nombre,
                UrlImagen = subCategory.UrlImagen,

            };
        }

        public static ViewDetailSubCategoryDto ToViewDetailSubCategoryDto(SubCategory subCategory)
        {
            return new ViewDetailSubCategoryDto()
            {
                Id = subCategory.Id,
                Nombre = subCategory.Nombre,
                UrlImagen = subCategory.UrlImagen,

            };
        }


    }
}
