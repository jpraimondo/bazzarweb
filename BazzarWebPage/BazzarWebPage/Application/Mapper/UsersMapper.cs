﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Core.Entities;

namespace BazzarWebPage.Application.Mapper
{
    public class UsersMapper
    {


        public static Users ToUsers(CreateUsersDto createUsers)
        {
            return new Users()
            {
                Name = createUsers.Name,
                Surname = createUsers.Surname,
                Email = createUsers.Email,
                Birthday = createUsers.Birthday,
                PhoneNumber = createUsers.PhoneNumber,

            };
        }
    }
}
