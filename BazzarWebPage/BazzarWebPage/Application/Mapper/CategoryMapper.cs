﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.DTOs.ViewDetail;
using BazzarWebPage.Core.Entities;

namespace BazzarWebPage.Application.Mapper
{
    public class CategoryMapper
    {

        public static Category ToCategory(CreateCategoryDto createCategory)
        {
            return new Category()
            {
                Nombre = createCategory.Nombre,

            };
        }

        public static ViewCategoryDto ToViewCategoryDto(Category category)
        {
            return new ViewCategoryDto()
            {
                Nombre = category.Nombre,
                UrlImagen = category.UrlImagen,
            };
        }

        public static ViewDetailCategoryDto ToViewDetailCategoryDto(Category category)
        {
            return new ViewDetailCategoryDto()
            {
                Id = category.Id,
                Nombre = category.Nombre,
                UrlImagen = category.UrlImagen,

            };
        }


    }
}
