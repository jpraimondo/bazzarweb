﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;

namespace BazzarWebPage.Application.Mapper
{
    public class NewsletterMapper
    {

        public static Newsletter ToNewsletter(CreateNewsletterDto createNewsletter)
        {
            return new Newsletter()
            {
                Correo = createNewsletter.Correo,
            };
        }

        public static ViewNewsletterDto ToViewNewsletterDto(Newsletter newsletter)
        {
            return new ViewNewsletterDto()
            {
                Id = newsletter.Id,
                Correo = newsletter.Correo,
            };
        }
    }
}
