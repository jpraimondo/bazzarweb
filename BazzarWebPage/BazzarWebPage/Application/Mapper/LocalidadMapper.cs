﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Core.Entities;

namespace Application.Mapper
{
    public class LocalidadMapper
    {

        public static Localidad ToLocalidad (CreateLocalidadDto createLocalidad)
        {
            return new Localidad()
            {
                Nombre = createLocalidad.Nombre,
                DepartamentoId = createLocalidad.DepartamentoId,

            };
        }

        public static ViewLocalidadDto ToViewLocalidadDto(Localidad localidad)
        {
            return new ViewLocalidadDto()
            {
                Id = localidad.Id,
                Nombre = localidad.Nombre,
            };
        }
    }
}
