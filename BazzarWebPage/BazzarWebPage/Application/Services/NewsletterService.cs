﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Application.Mapper;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Infraestructure.Services;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Services
{
    public class NewsletterService : INewsletterService
    {

        public IUnitOfWork unitOfWork;

        public NewsletterService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }


        public async Task<bool> Delete(int id)
        {
            var newsletter = await unitOfWork.NewsletterRepository.GetById(id);

            if (newsletter == null)
                return false;


            await unitOfWork.NewsletterRepository.Delete(newsletter);
            unitOfWork.Save();

            return true;
        }

        public IEnumerable<ViewNewsletterDto> Find(Expression<Func<Newsletter, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewNewsletterDto>> GetAll()
        {
            var newsletters = await unitOfWork.NewsletterRepository.GetAll();

            var newslettersDto = new List<ViewNewsletterDto>();

            foreach (Newsletter newsletter in newsletters)
            {
                newslettersDto.Add(NewsletterMapper.ToViewNewsletterDto(newsletter));
            }

            return newslettersDto;
        }

        public async Task<IEnumerable<ViewNewsletterDto>> GetAllAndDelete()
        {
            var newsletters = await unitOfWork.NewsletterRepository.GetAll();

            var newslettersDto = new List<ViewNewsletterDto>();

            foreach (Newsletter newsletter in newsletters)
            {
                newslettersDto.Add(NewsletterMapper.ToViewNewsletterDto(newsletter));
            }

            return newslettersDto;
        }

        public async Task<Newsletter> GetById(int id)
        {
            var newsletter = await unitOfWork.NewsletterRepository.GetById(id);

            return newsletter;
        }

        public async Task<Newsletter> Insert(CreateNewsletterDto newsletterDto)
        {
            var newsletter = NewsletterMapper.ToNewsletter(newsletterDto);
            newsletter.IsActve = true;
            await unitOfWork.NewsletterRepository.Insert(newsletter);
            await unitOfWork.Save();
            
            MailService email = new MailService();

            await email.EnviarCorreo(newsletter.Correo);


            return newsletter;
        }

        public async Task<Newsletter> Update(CreateNewsletterDto newsletterDto, int id)
        {
            var newsletter = await unitOfWork.NewsletterRepository.GetById(id);

            newsletter.Correo = newsletterDto.Correo;

            await unitOfWork.NewsletterRepository.Update(newsletter);

            unitOfWork.Save();

            return newsletter;
        }
    }
}
