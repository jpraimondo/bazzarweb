﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Application.Mapper;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Services
{
    public class DepartamentoService : IDepartamentoService
    {
        public IUnitOfWork unitOfWork;


        public DepartamentoService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }



        public async Task<bool> Delete(int id)
        {
            var departamento = await unitOfWork.DepartamentoRepository.GetById(id);

            if (departamento == null)
                return false;


            await unitOfWork.DepartamentoRepository.Delete(departamento);
            unitOfWork.Save();
            return true;
        }

        public IEnumerable<Departamento> Find(Expression<Func<Departamento, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewDepartamentoDto>> GetAll()
        {
            var departamentos = await unitOfWork.DepartamentoRepository.GetAll();

            var departamentosDto = new List<ViewDepartamentoDto>();

            foreach (Departamento departamento in departamentos)
            {
                departamentosDto.Add(DepartamentoMapper.ToViewDepartamentoDto(departamento));
            }

            return departamentosDto;
        }

        public async Task<IEnumerable<ViewDepartamentoDto>> GetAllAndDelete()
        {
            var departamentos = await unitOfWork.DepartamentoRepository.GetAllAndDelete();

            var departamentosDto = new List<ViewDepartamentoDto>();

            foreach (Departamento departamento in departamentos)
            {
                departamentosDto.Add(DepartamentoMapper.ToViewDepartamentoDto(departamento));
            }

            return departamentosDto;
        }

        public async Task<Departamento> GetById(int id)
        {
            var departamento = await unitOfWork.DepartamentoRepository.GetById(id);

            return departamento;
        }

        public async Task<Departamento> Insert(CreateDepartamentoDto departamentoDto)
        {
            var departamento = DepartamentoMapper.ToDepartamento(departamentoDto);

            await unitOfWork.DepartamentoRepository.Insert(departamento);

            unitOfWork.Save();

            return departamento;


        }

        public async Task<Departamento> Update(CreateDepartamentoDto departamentoDto, int id)
        {
            var departamento = await unitOfWork.DepartamentoRepository.GetById(id);

            departamento.Nombre = departamentoDto.Nombre;

            await unitOfWork.DepartamentoRepository.Update(departamento);

            unitOfWork.Save();

            return departamento;
        }
    }
}
