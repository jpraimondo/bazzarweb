﻿using Application.Mapper;
using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Services
{
    public class LocalidadService : ILocalidadService
    {

        public IUnitOfWork unitOfWork;

        public LocalidadService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }



        public async Task<bool> Delete(int id)
        {
            var localidad = await unitOfWork.LocalidadRepository.GetById(id);

            if (localidad == null)
                return false;


            await unitOfWork.LocalidadRepository.Delete(localidad);
            unitOfWork.Save();

            return true;
        }

        public IEnumerable<Localidad> Find(Expression<Func<Localidad, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewLocalidadDto>> GetAll()
        {
            var localidades = await unitOfWork.LocalidadRepository.GetAll();

            var localidadesDto = new List<ViewLocalidadDto>();

            foreach (Localidad localidad in localidades)
            {
                localidadesDto.Add(LocalidadMapper.ToViewLocalidadDto(localidad));
            }

            return localidadesDto;
        }

        public async Task<IEnumerable<ViewLocalidadDto>> GetAllAndDelete()
        {
            var localidades = await unitOfWork.LocalidadRepository.GetAll();

            var localidadesDto = new List<ViewLocalidadDto>();

            foreach (Localidad localidad in localidades)
            {
                localidadesDto.Add(LocalidadMapper.ToViewLocalidadDto(localidad));
            }

            return localidadesDto;
        }

        public async Task<Localidad> GetById(int id)
        {
            var localidad = await unitOfWork.LocalidadRepository.GetById(id);

            return localidad;
        }

        public async Task<Localidad> Insert(CreateLocalidadDto localidadDto)
        {
            var localidad = LocalidadMapper.ToLocalidad(localidadDto);

            localidad.Departamento = await unitOfWork.DepartamentoRepository.GetById(localidadDto.DepartamentoId);

            await unitOfWork.LocalidadRepository.Insert(localidad);

            unitOfWork.Save();

            return localidad;
        }

        public async Task<Localidad> Update(CreateLocalidadDto localidadDto, int id)
        {
            var localidad = await unitOfWork.LocalidadRepository.GetById(id);

            localidad.Nombre = localidadDto.Nombre;

            localidad.Departamento = await unitOfWork.DepartamentoRepository.GetById(localidadDto.DepartamentoId);


            await unitOfWork.LocalidadRepository.Update(localidad);

            unitOfWork.Save();

            return localidad;
        }
    }
}
