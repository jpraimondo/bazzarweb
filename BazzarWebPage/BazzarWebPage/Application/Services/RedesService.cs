﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.DTOs.ViewDetail;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Application.Mapper;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Services
{
    public class RedesService : IRedesService
    {
        public IUnitOfWork unitOfWork;

        public RedesService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Delete(int id)
        {
            var red = await unitOfWork.RedesRepository.GetById(id);

            if (red == null)
                return false;

            await unitOfWork.RedesRepository.Delete(red);
            _ = unitOfWork.Save();

            return true;
        }

        public IEnumerable<Redes> Find(Expression<Func<Redes, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewRedesDto>> GetAll()
        {
            var redes = await unitOfWork.RedesRepository.GetAll();

            var redesDto = new List<ViewRedesDto>();

            foreach (Redes red in redes)
            {
                redesDto.Add(RedesMapper.ToViewRedesDto(red));
            }

            return redesDto;
        }

        public async Task<IEnumerable<ViewRedesDto>> GetAllAndDelete()
        {
            var redes = await unitOfWork.RedesRepository.GetAllAndDelete();

            var redesDto = new List<ViewRedesDto>();

            foreach (Redes red in redes)
            {
                redesDto.Add(RedesMapper.ToViewRedesDto(red));
            }

            return redesDto;
        }

        public async Task<Redes> GetById(int id)
        {
            var red = await unitOfWork.RedesRepository.GetById(id);

            return red;
        }

        public async Task<ViewDetailRedesDto> Insert(CreateRedesDto redDto)
        {
            var red = RedesMapper.ToRedes(redDto);

            await unitOfWork.RedesRepository.Insert(red);

            _ = unitOfWork.Save();

            return RedesMapper.ToViewDetailRedesDto(red);
        }

        public async Task<ViewDetailRedesDto> Update(CreateRedesDto redDto, int id)
        {
            var red = await unitOfWork.RedesRepository.GetById(id);

            red.Link = redDto.Link;
            red.Nombre = redDto.Nombre;
            red.TiendaId = redDto.TiendaId;


            await unitOfWork.RedesRepository.Update(red);

            _ = unitOfWork.Save();

            return RedesMapper.ToViewDetailRedesDto(red);
        }
    }
}
