﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Application.Mapper;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Services
{
    public class TiendaService : ITiendaService
    {


        public IUnitOfWork unitOfWork;

        public TiendaService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }


        public async Task<bool> Delete(int id)
        {
            var tienda = await unitOfWork.TiendaRepository.GetById(id);

            if (tienda == null)
                return false;

            await unitOfWork.TiendaRepository.Delete(tienda);
            await unitOfWork.Save();

            return true;
        }

        public async Task<IEnumerable<ViewTiendaDto>> Find(Expression<Func<Tienda, bool>> predicate)
        {
            var tiendas = await unitOfWork.TiendaRepository.Find(predicate);

            var tiendasDto = new List<ViewTiendaDto>();

            foreach (Tienda tienda in tiendas)
            {
                tiendasDto.Add(TiendaMapper.ToViewTiendaDto(tienda));
            }


            return tiendasDto;
        }

        public async Task<IEnumerable<ViewTiendaDto>> GetAll()
        {
            var tiendas = await unitOfWork.TiendaRepository.GetAll();

            var tiendasDto = new List<ViewTiendaDto>();

            foreach (Tienda tienda in tiendas)
            {
                tiendasDto.Add(TiendaMapper.ToViewTiendaDto(tienda));
            }

            return tiendasDto;
        }

        public async Task<IEnumerable<ViewTiendaDto>> GetAllAndDelete()
        {
            var tiendas = await unitOfWork.TiendaRepository.GetAll();

            var tiendasDto = new List<ViewTiendaDto>();

            foreach (Tienda tienda in tiendas)
            {
                tiendasDto.Add(TiendaMapper.ToViewTiendaDto(tienda));
            }

            return tiendasDto;
        }

        public async Task<Tienda> GetById(int id)
        {
            var tienda = await unitOfWork.TiendaRepository.GetById(id);

            return tienda;
        }

        public async Task<Tienda> Insert(CreateTiendaDto tiendaDto)
        {
            var tienda = TiendaMapper.ToTienda(tiendaDto);

            tienda.Slug = await CreateSlug(tiendaDto.Nombre);

            tienda.Localidad = await unitOfWork.LocalidadRepository.GetById(tiendaDto.LocalidadId);

            tienda.SubCategoria = await unitOfWork.SubCategoryRepository.GetById(tiendaDto.SubCategoriaId);

            await unitOfWork.TiendaRepository.Insert(tienda);

            await unitOfWork.Save();

            return tienda;
        }

        public async Task<Tienda> Update(CreateTiendaDto tiendaDto, int id)
        {
            var tienda = await unitOfWork.TiendaRepository.GetById(id);

            tienda.Nombre = tiendaDto.Nombre;
            tienda.Descripcion = tiendaDto.Descripcion;
            tienda.Telefono = tiendaDto.Telefono;
            tienda.Slug = await CreateSlug(tiendaDto.Nombre);

            if (tienda.LocalidadId != tiendaDto.LocalidadId)
            {
                tienda.Localidad = await unitOfWork.LocalidadRepository.GetById(tiendaDto.LocalidadId);
                tienda.LocalidadId = tiendaDto.LocalidadId;
            }

            if (tienda.SubCategoriaId != tiendaDto.SubCategoriaId)
            {
                tienda.SubCategoria = await unitOfWork.SubCategoryRepository.GetById(tiendaDto.SubCategoriaId);
                tienda.SubCategoriaId = tiendaDto.SubCategoriaId;
            }

            await unitOfWork.TiendaRepository.Update(tienda);

            _ = unitOfWork.Save();

            return tienda;
        }

        private async Task<string> CreateSlug(string nombre)
        {

            string slug = nombre.Replace(" ", "-").ToLower();

            var tiendas = await unitOfWork.TiendaRepository.GetAllAndDelete();

            var tienda = tiendas.Where(t => t.Slug.Equals(slug)).FirstOrDefault();

            if (tienda == null)
            {
                return slug;
            }
            else
            {
                var random = new Random();
                var num = random.Next(0, 10000);

                slug = $"{slug}-{num}";

                return slug;
            }
        }
    }
}
