﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Application.Mapper;
using BazzarWebPage.Core.Constants;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Infraestructure.Services.Interface;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Services
{
    public class CategoryService : ICategoryService
    {
        public IUnitOfWork unitOfWork;
        public IFileHelper? fileHelper;

        public CategoryService(IUnitOfWork unitOfWork, IFileHelper fileHelper)
        {
            this.unitOfWork = unitOfWork;
            this.fileHelper = fileHelper;   
        }


        public async Task<bool> Delete(int id)
        {
            var category = await unitOfWork.CategoryRepository.GetById(id);

            if (category == null)
                return false;


            await unitOfWork.CategoryRepository.Delete(category);
            unitOfWork.Save();
            return true;

        }

        public IEnumerable<Category> Find(Expression<Func<Category, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewCategoryDto>> GetAll()
        {

            var categories = await unitOfWork.CategoryRepository.GetAll();

            var categoriesDto = new List<ViewCategoryDto>();

            foreach (Category category in categories)
            {
                categoriesDto.Add(CategoryMapper.ToViewCategoryDto(category));
            }

            return categoriesDto;
        }

        public async Task<IEnumerable<ViewCategoryDto>> GetAllAndDelete()
        {
            var categories = await unitOfWork.CategoryRepository.GetAllAndDelete();

            var categoriesDto = new List<ViewCategoryDto>();

            foreach (Category category in categories)
            {
                categoriesDto.Add(CategoryMapper.ToViewCategoryDto(category));
            }

            return categoriesDto;
        }

        public async Task<Category> GetById(int id)
        {
            var category = await unitOfWork.CategoryRepository.GetById(id);

            return category;
        }

        public async Task<Category> Insert(CreateCategoryDto categoryDto)
        {
            var category = CategoryMapper.ToCategory(categoryDto);

            if(categoryDto.UrlImagen != null)
            {
                category.UrlImagen = await fileHelper.AdjustSaveImage(categoryDto.UrlImagen,200,200,ConstantsApp.Contenedores.ConetenedorDeImagenesCategoria);
            }

            await unitOfWork.CategoryRepository.Insert(category);

            unitOfWork.Save();

            return category;

        }

        public async Task<Category> Update(CreateCategoryDto categoryDto, int id)
        {
            var category = await unitOfWork.CategoryRepository.GetById(id);

            category.Nombre = categoryDto.Nombre;

            if (categoryDto.UrlImagen != null)
            {
                category.UrlImagen = await fileHelper.AdjustSaveImage(categoryDto.UrlImagen, 200, 200, ConstantsApp.Contenedores.ConetenedorDeImagenesCategoria);
            };

            await unitOfWork.CategoryRepository.Update(category);

            unitOfWork.Save();

            return category;
        }

        public async Task<bool> Activate(int id)
        {
            try
            {
                var category = await unitOfWork.CategoryRepository.GetById(id);
                await unitOfWork.CategoryRepository.Activate(category);
                unitOfWork.Save();

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public async Task<bool> Inactivate(int id)
        {
            try
            {
                var category = await unitOfWork.CategoryRepository.GetById(id);
                await unitOfWork.CategoryRepository.Inactivate(category);
                unitOfWork.Save();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}
