﻿using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Services
{
    public class FotoService : IFotoService
    {
        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Foto> Find(Expression<Func<Foto, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Foto>> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Foto>> GetAllAndDelete()
        {
            throw new NotImplementedException();
        }

        public Task<Foto> GetById(int? id)
        {
            throw new NotImplementedException();
        }

        public Task<Foto> Insert(Foto contactDto)
        {
            throw new NotImplementedException();
        }

        public Task<Foto> Update(Foto contactDto, int id)
        {
            throw new NotImplementedException();
        }
    }
}
