﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.DTOs.View;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Application.Mapper;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using System.Linq.Expressions;

namespace BazzarWebPage.Application.Services
{
    public class SubCategoriaService : ISubCategoriaService
    {
        public IUnitOfWork unitOfWork;

        public SubCategoriaService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<bool> Delete(int id)
        {
            var subCategory = await unitOfWork.SubCategoryRepository.GetById(id);

            if (subCategory == null)
                return false;


            await unitOfWork.SubCategoryRepository.Delete(subCategory);
            unitOfWork.Save();
            return true;
        }

        public IEnumerable<SubCategory> Find(Expression<Func<SubCategory, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ViewSubCategoryDto>> GetAll()
        {
            var subCategories = await unitOfWork.SubCategoryRepository.GetAll();

            var subCategoriesDto = new List<ViewSubCategoryDto>();

            foreach (SubCategory subCategory in subCategories)
            {
                subCategoriesDto.Add(SubCategoryMapper.ToViewSubCategoryDto(subCategory));
            }

            return subCategoriesDto;
        }

        public async Task<IEnumerable<ViewSubCategoryDto>> GetAllAndDelete()
        {
            var subCategories = await unitOfWork.SubCategoryRepository.GetAllAndDelete();

            var subCategoriesDto = new List<ViewSubCategoryDto>();

            foreach (SubCategory subCategory in subCategories)
            {
                subCategoriesDto.Add(SubCategoryMapper.ToViewSubCategoryDto(subCategory));
            }

            return subCategoriesDto;
        }

        public async Task<SubCategory> GetById(int id)
        {
            var subCategory = await unitOfWork.SubCategoryRepository.GetById(id);

            return subCategory;
        }

        public async Task<SubCategory> Insert(CreateSubCategoryDto subCategoryDto)
        {
            var subCategory = SubCategoryMapper.ToSubCategory(subCategoryDto);


            subCategory.CategoriaPadre = await unitOfWork.CategoryRepository.GetById(subCategoryDto.CategoriaPadreId);

            await unitOfWork.SubCategoryRepository.Insert(subCategory);

            unitOfWork.Save();

            return subCategory;
        }

        public async Task<SubCategory> Update(CreateSubCategoryDto subCategoryDto, int id)
        {
            var subCategory = await unitOfWork.SubCategoryRepository.GetById(id);

            subCategory.Nombre = subCategoryDto.Nombre;

            if (subCategory.CategoriaPadreId != subCategoryDto.CategoriaPadreId)
            {
                subCategory.CategoriaPadreId = subCategoryDto.CategoriaPadreId;
                subCategory.CategoriaPadre = await unitOfWork.CategoryRepository.GetById(subCategoryDto.CategoriaPadreId);
            }


            await unitOfWork.SubCategoryRepository.Update(subCategory);

            unitOfWork.Save();

            return subCategory;
        }
    }
}
