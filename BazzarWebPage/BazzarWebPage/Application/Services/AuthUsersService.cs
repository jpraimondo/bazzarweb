﻿using BazzarWebPage.Application.DTOs.Create;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Application.Mapper;
using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace BazzarWebPage.Application.Services
{
    public class AuthUsersService: IAuthUsersService
    {

        public IUnitOfWork _unitOfWork;
   

        public AuthUsersService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
           
        }

        public async Task<Users> Register(CreateUsersDto createUsers)
        {
            var user = UsersMapper.ToUsers(createUsers);

            user.Localidad = await _unitOfWork.LocalidadRepository.GetById(createUsers.LocalidadId);

            user.RolId = 1;
            user.Rol = await _unitOfWork.RolsRepository.GetById((int)user.RolId);

            user.HashPassword = BCrypt.Net.BCrypt.HashPassword(createUsers.Password);

            await _unitOfWork.UsersRepository.Insert(user);

            await _unitOfWork.Save();

            return user;
        }


        public async Task<Users> Login(LoginDto userLogin)
        {

            try
            {

                var user = (await _unitOfWork.UsersRepository.Find(u => u.Email.Equals(userLogin.Email.Trim().ToLower()))).First();

               

                if ((user!=null) && BCrypt.Net.BCrypt.Verify(userLogin.Password, user.HashPassword))
                {
                    user.Rol = await _unitOfWork.RolsRepository.GetById((int)user.RolId);
                    return user;
                }
                else
                {
                    return new Users();
                }

            }
            catch (Exception)
            {

                return new Users();
            }
            
        }

        
    }
}
