﻿namespace BazzarWebPage.Application.DTOs.View
{
    public class ViewDepartamentoDto
    {
        public int Id { get; set; }

        public string? Nombre { get; set; }

        public IEnumerable<ViewLocalidadDto>? Localidades { get; set; }

    }
}
