﻿namespace BazzarWebPage.Application.DTOs.View
{
    public class ViewSubCategoryDto
    {
        public int Id { get; set; }

        public string? Nombre { get; set; }

        public string? UrlImagen { get; set; }
    }
}
