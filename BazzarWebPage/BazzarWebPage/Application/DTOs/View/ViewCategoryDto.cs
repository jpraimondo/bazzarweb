﻿namespace BazzarWebPage.Application.DTOs.View
{
    public class ViewCategoryDto
    {
        public int Id { get; set; }

        public string? Nombre { get; set; }

        public string? UrlImagen { get; set; }

        public virtual IEnumerable<ViewSubCategoryDto>? SubCategorias { get; set; }
    }
}
