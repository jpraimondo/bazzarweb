﻿

namespace BazzarWebPage.Application.DTOs.View
{
    public class ViewTiendaDto
    {
        public int Id { get; set; }

        public string? Nombre { get; set; }

        public string? Descripcion { get; set; }

        public string? Slug { get; set; }

        public string? Telefono { get; set; }

        public ViewLocalidadDto? Localidad { get; set; }

        public IEnumerable<ViewFotoDto>? Fotos { get; set; }

        public IEnumerable<ViewRedesDto>? RedesEmpresa { get; set; }
    }
}
