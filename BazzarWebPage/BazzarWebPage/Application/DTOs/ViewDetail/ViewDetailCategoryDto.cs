﻿namespace BazzarWebPage.Application.DTOs.ViewDetail
{
    public class ViewDetailCategoryDto
    {
        public int Id { get; set; }

        public string? Nombre { get; set; }

        public string? UrlImagen { get; set; }

        public virtual IEnumerable<ViewDetailSubCategoryDto>? SubCategorias { get; set; }
    }
}
