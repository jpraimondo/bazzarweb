﻿namespace BazzarWebPage.Application.DTOs.ViewDetail
{
    public class ViewDetailTiendaDto
    {
        public int Id { get; set; }

        public string? Nombre { get; set; }

        public string? Descripcion { get; set; }

        public string? Slug { get; set; }

        public string? Telefono { get; set; }

        public ViewDetailLocalidadDto? Localidad { get; set; }

        public IEnumerable<ViewDetailFotoDto>? Fotos { get; set; }

        public IEnumerable<ViewDetailRedesDto>? RedesEmpresa { get; set; }
    }
}
