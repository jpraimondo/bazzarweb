﻿namespace BazzarWebPage.Application.DTOs.ViewDetail
{
    public class ViewDetailSubCategoryDto
    {
        public int Id { get; set; }

        public string? Nombre { get; set; }

        public string? UrlImagen { get; set; }

        public IEnumerable<ViewDetailTiendaDto>? Tiendas { get; set; }
    }
}
