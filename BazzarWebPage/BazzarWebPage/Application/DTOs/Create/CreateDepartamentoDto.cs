﻿using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Application.DTOs.Create
{
    public class CreateDepartamentoDto
    {
        [Required]
        public string? Nombre { get; set; }
    }
}
