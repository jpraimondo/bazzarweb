﻿using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Application.DTOs.Create
{
    public class CreateTiendaDto
    {
        [Required]
        public string? Nombre { get; set; }

        [Required]
        public string? Descripcion { get; set; }

        [Required]
        public int LocalidadId { get; set; }

        [Required]
        public int SubCategoriaId { get; set; }

        [Required]
        public string? Telefono { get; set; }
    }
}
