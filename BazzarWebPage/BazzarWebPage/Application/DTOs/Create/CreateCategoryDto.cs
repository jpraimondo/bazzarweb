﻿using Microsoft.AspNetCore.Components.Forms;
using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Application.DTOs.Create
{
    public class CreateCategoryDto
    {

        [Required]
        public string? Nombre { get; set; }

        public IBrowserFile? UrlImagen { get; set; }


    }
}
