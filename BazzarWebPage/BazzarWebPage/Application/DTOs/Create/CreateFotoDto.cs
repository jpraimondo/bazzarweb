﻿using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Application.DTOs.Create
{
    public class CreateFotoDto
    {

        [Required]
        public string? UrlFoto { get; set; }

        [Required]
        public int Orden { get; set; }

        [Required]
        public int TiendaId { get; set; }

    }
}
