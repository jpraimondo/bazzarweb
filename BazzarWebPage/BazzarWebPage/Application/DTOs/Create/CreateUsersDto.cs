﻿using BazzarWebPage.Core.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Application.DTOs.Create
{
    public class CreateUsersDto
    {


        [Required]
        public string? Name { get; set; }

        [Required]
        public string? Surname { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public String? Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public String? Password { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public String? PhoneNumber { get; set; }

       
        [Required]
        [ForeignKey("Rol")]
        public int? RolId { get; set; }

        [Required]
        [ForeignKey("Localidad")]
        public int LocalidadId { get; set; }


    }
}
