﻿using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Application.DTOs.Create
{
    public class CreateNewsletterDto
    {

        [Required]
        [DataType(DataType.EmailAddress)]
        public string? Correo { get; set; }

    }
}
