﻿using Microsoft.AspNetCore.Components.Forms;
using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Application.DTOs.Create
{
    public class CreateSubCategoryDto
    {

        [Required]
        public string? Nombre { get; set; }

        public IBrowserFile? UrlImagen { get; set; }

        [Required]
        public int CategoriaPadreId { get; set; }

    }
}
