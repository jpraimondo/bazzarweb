﻿using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Application.DTOs.Create
{
    public class LoginDto
    {
        [Required]
        public string? Email { get; set; }
        [Required]
        public string? Password { get; set; }
    }
}
