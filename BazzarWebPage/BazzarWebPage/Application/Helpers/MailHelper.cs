﻿using Infraestructure.Services;
using Infraestructure.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BazzarWebPage.Application.Helpers
{
    internal class MailHelper
    {

        private IMailService MailService { get;}


        public MailHelper(IMailService mailService)
        {
            MailService = mailService;
        }


        public bool MailSender(string email)
        {
            try
            {

           var res = Regex.IsMatch( email,
                    @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                    RegexOptions.IgnoreCase);

            if (res)
                MailService.EnviarCorreo(email);

            return true;

            }
            catch (Exception)
            {

               return false;
            }


        }



    }
}
