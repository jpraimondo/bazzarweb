﻿using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Core.Constants;
using Infraestructure.Services.Interface;
using Microsoft.AspNetCore.Components.Forms;

namespace BazzarWebPage.Application.Helpers
{
    public class FileHelper : IFileHelper
    {

        private readonly IAlmacenadorArchivo almacenadorArchivo;

        public FileHelper(IAlmacenadorArchivo almacenadorArchivo)
        {
            this.almacenadorArchivo = almacenadorArchivo;
        }

        public void Dispose()
        {
            Dispose();
        }

        public async Task<string> SaveImage(IBrowserFile image, string? contenedor = "")
        {
            if (String.IsNullOrEmpty(contenedor))
                contenedor = ConstantsApp.Contenedores.ConetenedorDeImagenes;

            var memoryStream = new MemoryStream();

            await image.OpenReadStream(image.Size).CopyToAsync(memoryStream);
            byte[] fileByte = memoryStream.ToArray();
            string docUrl = await almacenadorArchivo.GuardarArchivo(fileByte, image.ContentType, Path.GetExtension(image.Name), contenedor, Guid.NewGuid().ToString());

            return docUrl;
        }
        public async Task<string> AdjustSaveImage(IBrowserFile image, int maxWidth, int maxHeight, string? contenedor = "")
        {

            if (maxHeight > 199 && maxWidth > 199)
                image = await image.RequestImageFileAsync(image.ContentType, maxWidth, maxHeight);
            else
                throw new Exception("El valor minimo de la imagen es 200x200");

            string docUrl = await SaveImage(image, contenedor);

            return docUrl;
        }
    }
}
