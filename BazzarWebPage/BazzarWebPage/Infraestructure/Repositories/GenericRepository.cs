﻿using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BazzarWebPage.Infraestructure.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : EntityBase
    {
        protected readonly AppDbContext context;
        protected DbSet<T> entities;

        public GenericRepository(AppDbContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }

        public async Task Insert(T entity)
        {
            entity.CreateTime = DateTime.Now;
            entity.IsDelete = false;

            await entities.AddAsync(entity);
        }
        public async Task Update(T entity)
        {
            entity.EditTime = DateTime.Now;

            entities.Update(entity);
        }
        public async Task Delete(T entity)
        {
            //entity.DeleteTime = DateTime.Now;
            entity.IsDelete = true;

            entities.Update(entity);
        }

        public async Task<T> GetById(int id)
        {
            T? t = await entities.FindAsync(id);

            return t;

        }

        public async Task<IEnumerable<T>> GetAllAndDelete()
        {
           
                return await entities.ToListAsync();
           
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            
                return await entities.Where(e => e.IsDelete.Equals(false)).ToListAsync();
            
        }

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate)
        {
            return await entities.Where(predicate).ToListAsync();
        }

        public async Task Activate(T entity)
        {
            T? t = await entities.FindAsync(entity.Id);

            t.IsActve = true;

            entities.Update(t);

        }

        public async Task Inactivate(T entity)
        {
            T? t = await entities.FindAsync(entity.Id);

            t.IsActve = false;

            entities.Update(t);

        }
    }
}
