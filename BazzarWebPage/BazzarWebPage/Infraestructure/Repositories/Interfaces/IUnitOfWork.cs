﻿using BazzarWebPage.Core.Entities;

namespace BazzarWebPage.Infraestructure.Repositories.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        public IGenericRepository<Category> CategoryRepository { get; }
        public IGenericRepository<Departamento> DepartamentoRepository { get; }
        public IGenericRepository<Foto> FotoRepository { get; }
        public IGenericRepository<Localidad> LocalidadRepository { get; }
        public IGenericRepository<Newsletter> NewsletterRepository { get; }
        public IGenericRepository<Redes> RedesRepository { get; }
        public IGenericRepository<SubCategory> SubCategoryRepository { get; }
        public IGenericRepository<Tienda> TiendaRepository { get; }
        public IGenericRepository<Rols> RolsRepository { get; }
        public IGenericRepository<Users> UsersRepository { get; }

        Task Save();
    }
}
