﻿using BazzarWebPage.Core.Entities;
using System.Linq.Expressions;

namespace BazzarWebPage.Infraestructure.Repositories.Interfaces
{
    public interface IGenericRepository<T> where T : EntityBase
    {
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> GetAllAndDelete();
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate);
        Task Activate(T entity);
        Task Inactivate(T entity);
        Task Insert(T entity);
        Task Update(T entity);
        Task Delete(T entity);
        Task<T> GetById(int id);
    }
}
