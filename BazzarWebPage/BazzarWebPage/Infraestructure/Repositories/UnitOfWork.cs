﻿using BazzarWebPage.Core.Entities;
using BazzarWebPage.Infraestructure.Persistence;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace BazzarWebPage.Infraestructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly AppDbContext context;

        public IGenericRepository<Category>? categoryRepository;

        public IGenericRepository<Departamento>? departamentoRepository;

        public IGenericRepository<Foto>? fotoRepository;

        public IGenericRepository<Localidad>? localidadRepository;

        public IGenericRepository<Newsletter>? newsletterRepository;

        public IGenericRepository<Redes>? redesRepository;

        public IGenericRepository<SubCategory>? subCategoryRepository;

        public IGenericRepository<Tienda>? tiendaRepository;

        public IGenericRepository<Rols>? rolsRepository;

        public IGenericRepository<Users>? usersRepository;

        public UnitOfWork(AppDbContext ctx)
        {
            context = ctx;
        }

        public IGenericRepository<Category> CategoryRepository
        {
            get
            {
                if (categoryRepository == null)
                {
                    categoryRepository = new GenericRepository<Category>(context);
                }
                return categoryRepository;
            }
        }

        public IGenericRepository<Departamento> DepartamentoRepository
        {
            get
            {
                if (departamentoRepository == null)
                {
                    departamentoRepository = new GenericRepository<Departamento>(context);
                }
                return departamentoRepository;
            }
        }

        public IGenericRepository<Foto> FotoRepository
        {
            get
            {
                if (fotoRepository == null)
                {
                    fotoRepository = new GenericRepository<Foto>(context);
                }
                return fotoRepository;
            }
        }

        public IGenericRepository<Localidad> LocalidadRepository
        {
            get
            {
                if (localidadRepository == null)
                {
                    localidadRepository = new GenericRepository<Localidad>(context);
                }
                return localidadRepository;
            }
        }

        public IGenericRepository<Newsletter> NewsletterRepository
        {
            get
            {
                if (newsletterRepository == null)
                {
                    newsletterRepository = new GenericRepository<Newsletter>(context);
                }
                return newsletterRepository;
            }
        }

        public IGenericRepository<Redes> RedesRepository
        {
            get
            {
                if (redesRepository == null)
                {
                    redesRepository = new GenericRepository<Redes>(context);
                }
                return redesRepository;
            }
        }

        public IGenericRepository<SubCategory> SubCategoryRepository
        {
            get
            {
                if (subCategoryRepository == null)
                {
                    subCategoryRepository = new GenericRepository<SubCategory>(context);
                }
                return subCategoryRepository;
            }
        }

        public IGenericRepository<Tienda> TiendaRepository
        {
            get
            {
                if (tiendaRepository == null)
                {
                    tiendaRepository = new GenericRepository<Tienda>(context);
                }
                return tiendaRepository;
            }
        }

        public IGenericRepository<Rols> RolsRepository
        {
            get
            {
                if (rolsRepository == null)
                {
                    rolsRepository = new GenericRepository<Rols>(context);
                }
                return rolsRepository;
            }
        }

        public IGenericRepository<Users> UsersRepository 
        { 
            get
            {
                if (usersRepository == null)
                {
                    usersRepository = new GenericRepository<Users>(context);
                }
                return usersRepository;
            }
        }


        public void Dispose() { context.Dispose(); }

        public async Task Save()
        {
            await context.SaveChangesAsync();
        }
    }
}
