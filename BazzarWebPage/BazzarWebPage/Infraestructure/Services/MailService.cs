﻿using Infraestructure.Services.Interface;
using SendGrid.Helpers.Mail;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Services
{
    public class MailService:IMailService
    {
        public string key = "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";
        

        public async Task EnviarCorreo(string email)
        {
            var apiKey = $"{key}";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("contacto@quammit.com", "NoReply");
            var subject = "Sending with SendGrid is Fun";
            var to = new EmailAddress(email, "Invitado");
            var plainTextContent = "and easy to do anywhere, even with C#";
            var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = await client.SendEmailAsync(msg);
        }
    }
}
