﻿namespace Infraestructure.Services.Interface
{
    public interface IAlmacenadorArchivo
    {

        public Task<string> GuardarArchivo(byte[] file, string contentType, string extension, string contenedor, string nombre);

        public Task BorrarArchivo(string ruta, string contenedor);
    }
}
