﻿using BazzarWebPage.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Emit;

namespace BazzarWebPage.Infraestructure.Persistence
{
    public class AppDbContext : DbContext
    {

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Category>? Categorias { get; set; }
        public DbSet<SubCategory>? SubCategorias { get; set; }
        public DbSet<Departamento>? Departamentos { get; set; }
        public DbSet<Localidad>? Localidades { get; set; }
        public DbSet<Newsletter>? Newsletters { get; set; }
        public DbSet<Foto>? Fotos { get; set; }
        public DbSet<Tienda>? Tiendas { get; set; }
        public DbSet<Redes>? Redes { get; set; }
        public DbSet<Rols>? Rols { get; set; }
        public DbSet<Users>? Users { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.HasDefaultSchema("bazzardb");

            builder.Entity<Category>().ToTable("categorias").HasKey(c => c.Id);
            builder.Entity<SubCategory>().ToTable("subcategorias").HasKey(s => s.Id);
            builder.Entity<Departamento>().ToTable("departamentos").HasKey(d => d.Id);
            builder.Entity<Localidad>().ToTable("localidades").HasKey(l => l.Id);
            builder.Entity<Newsletter>().ToTable("newsletters").HasKey(n => n.Id);
            builder.Entity<Foto>().ToTable("fotos").HasKey(f => f.Id);
            builder.Entity<Tienda>().ToTable("tiendas").HasKey(t => t.Id);
            builder.Entity<Redes>().ToTable("redes").HasKey(r => r.Id);
            builder.Entity<Rols>().ToTable("rols").HasKey(r => r.Id);
            builder.Entity<Users>().ToTable("users").HasKey(u => u.Id);


            builder.Seed();
    }

    }
}
