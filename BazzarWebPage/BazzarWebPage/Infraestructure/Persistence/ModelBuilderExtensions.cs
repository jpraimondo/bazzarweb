﻿using BazzarWebPage.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace BazzarWebPage.Infraestructure.Persistence
{
    public static class ModelBuilderExtensions
	{

		public static void Seed(this ModelBuilder modelBuilder)
		{

			modelBuilder.Entity<Category>()
				.HasData(
                new Category()
                {
                    Id = 1,
                    IsActve = true,
                    IsDelete = false,
                    Nombre = "categoria 1",
                    UrlImagen = "Url de la categoria",

                },
                new Category()
                {
                    Id = 2,
                    IsActve = true,
                    IsDelete = false,
                    Nombre = "categoria 2",
                    UrlImagen = "Url de la categoria",

                }, new Category()
                {
                    Id = 3,
                    IsActve = true,
                    IsDelete = false,
                    Nombre = "categoria 3",
                    UrlImagen = "Url de la categoria",

                });


            modelBuilder.Entity<Departamento>()
                .HasData(
                new Departamento() { Id = 1, IsActve = true, Nombre = "Montevideo", IsDelete = false },
                new Departamento() { Id = 2, IsActve = true, Nombre = "Canelones", IsDelete = false },
                new Departamento() { Id = 3, IsActve = true, Nombre = "Maldonado", IsDelete = false },
                new Departamento() { Id = 4, IsActve = true, Nombre = "Rocha", IsDelete = false },
                new Departamento() { Id = 5, IsActve = true, Nombre = "Colonia", IsDelete = false },
                new Departamento() { Id = 6, IsActve = true, Nombre = "San Jose", IsDelete = false },
                new Departamento() { Id = 7, IsActve = true, Nombre = "Flores", IsDelete = false },
                new Departamento() { Id = 8, IsActve = true, Nombre = "Lavalleja", IsDelete = false },
                new Departamento() { Id = 9, IsActve = true, Nombre = "Treinta y tres", IsDelete = false },
                new Departamento() { Id = 10, IsActve = true, Nombre = "Cerro Largo", IsDelete = false },
                new Departamento() { Id = 11, IsActve = true, Nombre = "Durazno", IsDelete = false },
                new Departamento() { Id = 12, IsActve = true, Nombre = "Tacuarembo", IsDelete = false },
                new Departamento() { Id = 13, IsActve = true, Nombre = "Rivera", IsDelete = false },
                new Departamento() { Id = 14, IsActve = true, Nombre = "Artigas", IsDelete = false },
                new Departamento() { Id = 15, IsActve = true, Nombre = "Salto", IsDelete = false },
                new Departamento() { Id = 16, IsActve = true, Nombre = "Paysandú", IsDelete = false },
                new Departamento() { Id = 17, IsActve = true, Nombre = "Rio Negro", IsDelete = false },
                new Departamento() { Id = 18, IsActve = true, Nombre = "Soriano", IsDelete = false },
                new Departamento() { Id = 19, IsActve = true, Nombre = "Florida", IsDelete = false });


            modelBuilder.Entity<Localidad>()
               .HasData(
                new Localidad() { Id = 1, IsActve = true, Nombre = "Montevideo", IsDelete = false, DepartamentoId = 1 },
                new Localidad() { Id = 2, IsActve = true, Nombre = "Canelones", IsDelete = false, DepartamentoId = 2 },
                new Localidad() { Id = 3, IsActve = true, Nombre = "Ciudad de la Costa", IsDelete = false, DepartamentoId = 2 },
                new Localidad() { Id = 4, IsActve = true, Nombre = "Atlantida", IsDelete = false, DepartamentoId = 2 },
                new Localidad() { Id = 5, IsActve = true, Nombre = "Maldonado", IsDelete = false, DepartamentoId = 3 },
                new Localidad() { Id = 6, IsActve = true, Nombre = "Punta del Este", IsDelete = false, DepartamentoId = 3 },
                new Localidad() { Id = 7, IsActve = true, Nombre = "Piriapolis", IsDelete = false, DepartamentoId = 3 });

            modelBuilder.Entity<Newsletter>()
              .HasData(
                new Newsletter() { Id = 1, IsActve = true, Correo = "correo1@empresa.com", IsDelete = false },
                new Newsletter() { Id = 2, IsActve = true, Correo = "correo2@empresa.com", IsDelete = false },
                new Newsletter() { Id = 3, IsActve = true, Correo = "correo3@empresa.com", IsDelete = false },
                new Newsletter() { Id = 4, IsActve = true, Correo = "correo4@empresa.com", IsDelete = false },
                new Newsletter() { Id = 5, IsActve = true, Correo = "correo5@empresa.com", IsDelete = false },
                new Newsletter() { Id = 6, IsActve = true, Correo = "correo6@empresa.com", IsDelete = false },
                new Newsletter() { Id = 7, IsActve = true, Correo = "correo7@empresa.com", IsDelete = false });


            modelBuilder.Entity<Redes>()
             .HasData(
                new Redes() { Id = 1, IsActve = true, Nombre = "Instagram", Link = "http://instagram.com/tienda1", TiendaId = 1, IsDelete = false },
                new Redes() { Id = 2, IsActve = true, Nombre = "Facebook", Link = "http://facebook.com/tienda1", TiendaId = 1, IsDelete = false },
                new Redes() { Id = 3, IsActve = true, Nombre = "Instagram", Link = "http://instagram.com/tienda2", TiendaId = 2, IsDelete = false },
                new Redes() { Id = 4, IsActve = true, Nombre = "Facebook", Link = "http://facebook.com/tienda2", TiendaId = 2, IsDelete = false },
                new Redes() { Id = 5, IsActve = true, Nombre = "Instagram", Link = "http://instagram.com/tienda3", TiendaId = 3, IsDelete = false },
                new Redes() { Id = 6, IsActve = true, Nombre = "Facebook", Link = "http://facebook.com/tienda3", TiendaId = 3, IsDelete = false },
                new Redes() { Id = 7, IsActve = true, Nombre = "Facebook", Link = "http://facebook.com/tienda4", TiendaId = 4, IsDelete = false });

            modelBuilder.Entity<SubCategory>()
             .HasData(
                new SubCategory() { Id = 1, IsActve = true, IsDelete = false, Nombre = "SubCategoria 1", UrlImagen = "Url de la SubCategoria", CategoriaPadreId = 1 },
                new SubCategory() { Id = 2, IsActve = true, IsDelete = false, Nombre = "SubCategoria 2", UrlImagen = "Url de la SubCategoria", CategoriaPadreId = 1 },
                new SubCategory() { Id = 3, IsActve = true, IsDelete = false, Nombre = "SubCategoria 3", UrlImagen = "Url de la SubCategoria", CategoriaPadreId = 2 },
                new SubCategory() { Id = 4, IsActve = true, IsDelete = false, Nombre = "SubCategoria 4", UrlImagen = "Url de la SubCategoria", CategoriaPadreId = 2 },
                new SubCategory() { Id = 5, IsActve = true, IsDelete = false, Nombre = "SubCategoria 5", UrlImagen = "Url de la SubCategoria", CategoriaPadreId = 2 },
                new SubCategory() { Id = 6, IsActve = true, IsDelete = false, Nombre = "SubCategoria 6", UrlImagen = "Url de la SubCategoria", CategoriaPadreId = 2 },
                new SubCategory() { Id = 7, IsActve = true, IsDelete = false, Nombre = "SubCategoria 7", UrlImagen = "Url de la SubCategoria", CategoriaPadreId = 3 },
                new SubCategory() { Id = 8, IsActve = true, IsDelete = false, Nombre = "SubCategoria 8", UrlImagen = "Url de la SubCategoria", CategoriaPadreId = 3 });

            modelBuilder.Entity<Tienda>()
             .HasData(
                 new Tienda() { Id = 1, IsActve = true, IsDelete = false, Nombre = "Tienda 1", Descripcion = "Descripcion Tienda 1", SubCategoriaId = 1, LocalidadId = 1, Slug = "http://empresa.com/tienda-1" },
                new Tienda() { Id = 2, IsActve = true, IsDelete = false, Nombre = "Tienda 2", Descripcion = "Descripcion Tienda 2", SubCategoriaId = 1, LocalidadId = 1, Slug = "http://empresa.com/tienda-2" },
                new Tienda() { Id = 3, IsActve = true, IsDelete = false, Nombre = "Tienda 3", Descripcion = "Descripcion Tienda 3", SubCategoriaId = 2, LocalidadId = 5, Slug = "http://empresa.com/tienda-3" },
                new Tienda() { Id = 4, IsActve = true, IsDelete = false, Nombre = "Tienda 4", Descripcion = "Descripcion Tienda 4", SubCategoriaId = 2, LocalidadId = 5, Slug = "http://empresa.com/tienda-4" },
                new Tienda() { Id = 5, IsActve = true, IsDelete = false, Nombre = "Tienda 5", Descripcion = "Descripcion Tienda 5", SubCategoriaId = 2, LocalidadId = 4, Slug = "http://empresa.com/tienda-5" },
                new Tienda() { Id = 6, IsActve = true, IsDelete = false, Nombre = "Tienda 6", Descripcion = "Descripcion Tienda 6", SubCategoriaId = 2, LocalidadId = 4, Slug = "http://empresa.com/tienda-6" },
                new Tienda() { Id = 7, IsActve = true, IsDelete = false, Nombre = "Tienda 7", Descripcion = "Descripcion Tienda 7", SubCategoriaId = 3, LocalidadId = 5, Slug = "http://empresa.com/tienda-7" },
                new Tienda() { Id = 8, IsActve = true, IsDelete = false, Nombre = "Tienda 8", Descripcion = "Descripcion Tienda 8", SubCategoriaId = 3, LocalidadId = 4, Slug = "http://empresa.com/tienda-8" });



            modelBuilder.Entity<Rols>()
             .HasData(
                 new Rols() { Id = 1, IsActve = true, IsDelete = false, Name = "Usuario" },
                new Rols() { Id = 2, IsActve = true, IsDelete = false, Name = "Administrador"}
               );




            modelBuilder.Entity<Users>()
            .HasData(
                new Users()
                {
                    Id = 1,
                    Name = "Juan",
                    Surname = "Perez",
                    Email = "jperez@correo.com",
                    HashPassword = BCrypt.Net.BCrypt.HashPassword("MiPassword"),
                    Birthday = DateTime.Now.AddYears(-32),
                    PhoneNumber = "24622451",
                    LocalidadId = 1,
                    IsActve = true,
                    IsDelete = false,
                    RolId =1
                },
                new Users()
                {
                    Id = 2,
                    Name = "Mario",
                    Surname = "Gomez",
                    Email = "mgomez@correo.com",
                    HashPassword = BCrypt.Net.BCrypt.HashPassword("MiPassword"),
                    Birthday = DateTime.Now.AddYears(-38),
                    PhoneNumber = "24622451",
                    LocalidadId = 1,
                    IsActve = true,
                    IsDelete = false,
                    RolId =2
                },
                new Users()
                {
                    Id = 3,
                    Name = "Eliana",
                    Surname = "Romero",
                    Email = "eromero@correo.com",
                    HashPassword = BCrypt.Net.BCrypt.HashPassword("MiPassword"),
                    Birthday = DateTime.Now.AddYears(-26),
                    PhoneNumber = "24622451",
                    LocalidadId=1,
                    IsActve = true,
                    IsDelete = false,
                    RolId = 2
                }) ;



            
        }


    }
}
