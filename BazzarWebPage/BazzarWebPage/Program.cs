using BazzarWebPage.Application.Helpers;
using BazzarWebPage.Application.Interfaces;
using BazzarWebPage.Application.Services;
using BazzarWebPage.Infraestructure.Repositories.Interfaces;
using BazzarWebPage.Infraestructure.Repositories;
using BazzarWebPage.Infraestructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Infraestructure.Services.Interface;
using Infraestructure.Services;
using Microsoft.AspNetCore.Components.Authorization;
using BazzarWebPage.Application.Authentication;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddAuthenticationCore();
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<AppDbContext>(
    options => options.UseSqlServer(connectionString));

builder.Services.AddHttpContextAccessor();



#region Authentication
builder.Services.AddScoped<ProtectedSessionStorage>();
builder.Services.AddScoped<IAuthUsersService, AuthUsersService>();
builder.Services.AddScoped<AuthenticationStateProvider, ApplicationAuthenticationStateProvider>();

#endregion

#region Infraestructure

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddSingleton<IAlmacenadorArchivo, AlmacenadorArchivo>();

#endregion

#region Application


builder.Services.AddSingleton<IFileHelper, FileHelper>();
builder.Services.AddScoped<ICategoryService, CategoryService>();
builder.Services.AddScoped<INewsletterService, NewsletterService>();

#endregion


////Manejo de authenticacion
//builder.Services.AddSingleton<AuthService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseAuthentication();
app.UseAuthorization();

app.UseRouting();
app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();
