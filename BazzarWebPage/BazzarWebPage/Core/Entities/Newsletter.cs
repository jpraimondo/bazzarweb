﻿using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Core.Entities
{
    public class Newsletter : EntityBase
    {

        [Required]
        [DataType(DataType.EmailAddress)]
        public string? Correo { get; set; }



    }
}
