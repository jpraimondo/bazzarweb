﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BazzarWebPage.Core.Entities
{
    public class Redes : EntityBase
    {

        [Required]
        public string? Nombre { get; set; }
        [Required]
        public string? Link { get; set; }

        [ForeignKey("Tienda")]
        public int TiendaId { get; set; }
        public virtual Tienda? Tienda { get; set; }
    }
}
