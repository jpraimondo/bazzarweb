﻿using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Core.Entities
{
    public class Departamento : EntityBase
    {
        [Required]
        public string? Nombre { get; set; }

        public virtual IEnumerable<Localidad>? Localidades { get; set; }
    }
}
