﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BazzarWebPage.Core.Entities
{
    public class EntityBase
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime EditTime { get; set; }

        public DateTime DeleteTime { get; set; }

        public bool IsActve { get; set; }

        public bool IsDelete { get; set; }
    }
}
