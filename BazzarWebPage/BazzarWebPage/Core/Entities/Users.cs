﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BazzarWebPage.Core.Entities
{
    public class Users:EntityBase
    {

        [Required]
        public string? Name { get; set; }

        [Required]
        public string? Surname { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public String? Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public String? HashPassword { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public String? PhoneNumber { get; set; }

       
        [Required]
        [ForeignKey("Rol")]
        public int? RolId { get; set; }
        public virtual Rols? Rol { get; set; }

       
        [ForeignKey("Localidad")]
        public int LocalidadId { get; set; }
        public virtual Localidad? Localidad { get; set; }

       
        [ForeignKey("Tienda")]
        public int StoreId { get; set; }
        public virtual IEnumerable<Tienda>? Stores { get; set; }



    }
}
