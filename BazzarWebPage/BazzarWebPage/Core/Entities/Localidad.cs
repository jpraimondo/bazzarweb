﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BazzarWebPage.Core.Entities
{
    public class Localidad : EntityBase
    {
        [Required]
        public string? Nombre { get; set; }

        [Required]
        [ForeignKey("Departamento")]
        public int DepartamentoId { get; set; }
        public virtual Departamento? Departamento { get; set; }


        public IList<Tienda>? Tiendas { get; set; }

    }
}
