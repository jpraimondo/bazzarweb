﻿using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Core.Entities
{
    public class Category : EntityBase
    {

        [Required]
        public string? Nombre { get; set; }

        public string? UrlImagen { get; set; }

        public virtual IEnumerable<SubCategory>? SubCategorias { get; set; }


    }
}
