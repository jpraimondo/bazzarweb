﻿using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Core.Entities
{
    public class SubCategory : EntityBase
    {

        [Required]
        public string? Nombre { get; set; }

        public string? UrlImagen { get; set; }

        [Required]
        public int? CategoriaPadreId { get; set; }
        public virtual Category? CategoriaPadre { get; set; }

        public virtual IEnumerable<Tienda>? Tiendas { get; set; }
    }
}
