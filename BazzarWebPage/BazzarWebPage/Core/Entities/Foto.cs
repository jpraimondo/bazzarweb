﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BazzarWebPage.Core.Entities

{
    public class Foto : EntityBase
    {
        [Required]
        public string? UrlFoto { get; set; }

        [Required]
        public int Orden { get; set; }

    }
}
