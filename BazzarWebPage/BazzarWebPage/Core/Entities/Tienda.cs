﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BazzarWebPage.Core.Entities
{
    public class Tienda : EntityBase
    {

        [Required]
        public string? Nombre { get; set; }

        public string? Descripcion { get; set; }

        [Required]
        public string? Slug { get; set; }

        [ForeignKey("SubCategoria")]
        public int SubCategoriaId { get; set; }
        public virtual SubCategory? SubCategoria { get; set; }

        [Required]
        [ForeignKey("Localidad")]
        public int LocalidadId { get; set; }
        public virtual Localidad? Localidad { get; set; }


        public virtual IEnumerable<Foto>? Fotos { get; set; }

        public bool Estado { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string? Telefono { get; set; }

        public virtual IEnumerable<Redes>? RedesEmpresa { get; set; }


    }
}
