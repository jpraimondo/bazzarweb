﻿using Microsoft.AspNetCore.Components.Forms;
using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Core.Validations
{
    public class PesoArchivoAttribute : ValidationAttribute
    {

        private readonly double _pesoArchivoKb;

        public PesoArchivoAttribute(double pesoArchivoKb)
        {
            _pesoArchivoKb = pesoArchivoKb;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var browserFile = value as IBrowserFile;

            if (browserFile != null)

            {
                if (browserFile.Size / 1024 > _pesoArchivoKb)
                {
                    return new ValidationResult($"El peso maximo del archivo es de {_pesoArchivoKb} KB");
                }
            }

            return ValidationResult.Success;
        }


    }
}
