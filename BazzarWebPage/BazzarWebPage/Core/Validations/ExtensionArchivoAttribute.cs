﻿using Microsoft.AspNetCore.Components.Forms;
using System.ComponentModel.DataAnnotations;

namespace BazzarWebPage.Core.Validations
{
    public class ExtensionArchivoAttribute : ValidationAttribute
    {
        private readonly string[] _tiposValidos;

        public ExtensionArchivoAttribute(string[] tiposValidos)
        {
            _tiposValidos = tiposValidos;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                var formfile = value as IBrowserFile;

                if (formfile != null)
                {
                    if (!_tiposValidos.Contains(formfile.ContentType))
                    {
                        return new ValidationResult($"Los tipos validos son {string.Join(",", _tiposValidos)}");
                    }
                }

                return ValidationResult.Success;
            }
            catch (ValidationException ex)
            {

                throw new Exception(ex.Message);
            }


        }
    }
}
