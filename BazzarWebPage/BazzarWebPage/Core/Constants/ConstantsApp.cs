﻿namespace BazzarWebPage.Core.Constants
{
    public class ConstantsApp
    {

        public static class Contenedores
        {
            public const string ConetenedorDeDocumentos = "docs";

            public const string ConetenedorDeImagenes = "img/public";

            public const string ConetenedorDeImagenesCategoria = "img/category";

            public const string ConetenedorDeImagenesTienda = "img/store";
        }

    }
}
